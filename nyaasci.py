#!/usr/bin/env python3
import curses
import random
import time
import math
import sys

import assets

framerate = 1 / 20

def debug(*args):
    with open ('debug', 'w') as f:
        f.write(' '.join(str(arg) for arg in args))

class SpriteGroup:
    '''Groups multiple sprites together'''
    def __init__(self, frame, stdscr):
        self.sprites = []
        self.done = False

    def add(self, sprite):
        self.sprites.append(sprite)

    def draw(self, frame, stdscr):
        for sprite in self.sprites:
            sprite.draw(frame, stdscr)

        self.sprites = [sprite for sprite in self.sprites if not sprite.done]

class Starfield(SpriteGroup):
    '''Creates new Starburst sprites every frame'''
    def draw(self, frame, stdscr):
        self.add(Starburst(frame, stdscr))
        super().draw(frame, stdscr)

class Sprite:
    '''Base class for all sprites'''
    def __init__(self, frame, stdscr):
        self.frames = []
        self.position = [0, 0]
        self.done = False

    def get_frame_content(self, frame):
        return self.frames[frame % len(self.frames)].strip('\n')

    def get_frame_size(self, frame):
        frame = self.get_frame_content(frame)
        lines = frame.split('\n')
        return (len(lines), len(lines[0]))

    def draw(self, frame, stdscr):
        if self.done: return

        # Writes frame content line-by-line, exluding whitespace preceeding content to simulate transparency
        content = self.get_frame_content(frame)
        for i, line in enumerate(content.split('\n')):
            skip = len(line) - len(line.lstrip(' '))
            stdscr.addstr(self.position[0] + i, self.position[1] + skip, line[skip:].rstrip(' '))

class Cat(Sprite):
    '''Nyan!'''
    def __init__(self, frame, stdscr):
        super().__init__(frame, stdscr)
        self.frames = assets.cat_frames

        ssize = stdscr.getmaxyx()
        fsize = self.get_frame_size(0)
        # Center of screen
        self.position = [
            int(ssize[0] / 2 - fsize[0] / 2),
            int(ssize[1] / 2 - fsize[1] / 2)
        ]

    '''Make this sprite update frame at slower rate'''
    def get_frame_content(self, frame):
        return super().get_frame_content(int(frame / 3))

class Starburst(Sprite):
    def __init__(self, frame, stdscr):
        super().__init__(frame, stdscr)
        self.frames = assets.starburst_frames

        ssize = stdscr.getmaxyx()
        fsize = self.get_frame_size(0)
        # Random position, ensures all of frame is in bounds during whole animation
        self.position = [
            random.randint(
                int(fsize[0] / 2) + fsize[0],
                int(ssize[0] - fsize[0])),
            random.randint(
                int(fsize[1] / 2),
                int(ssize[1] - fsize[1] - 1))
        ]

        self.start_frame = frame

    def draw(self, frame, stdscr):
        debug(stdscr.getmaxyx(), self.position)
        super().draw(frame, stdscr)

        # Move starburst to left each frame
        self.position[1] -= 1
        if frame - self.start_frame >= len(self.frames) or self.position[1] < 0:
            self.done = True

class Trail(Sprite):
    '''Rainbows kinda lose their pizzaz in monochrome'''
    def __init__(self, frame, stdscr):
        super().__init__(frame, stdscr)

        ssize = stdscr.getmaxyx()
        fsize = Cat(0, stdscr).get_frame_size(0)
        self.position = [
            int(ssize[0] / 2 - fsize[0] / 2) + 1,
            0
        ]

        # Create repeating trail frames from trail asset
        trail_lines = assets.trail.strip('\n').split('\n')
        num_frames = len(trail_lines[0])

        trail_len = int(ssize[1] / 2 - fsize[1] / 2) + 5
        repeats = math.ceil(trail_len / len(trail_lines[0]))

        frames_lines = [[] for x in range(num_frames)]
        for line in trail_lines:
            repeated = line * repeats
            for frame in range(num_frames):
                frames_lines[frame].append(repeated[frame:trail_len+frame])

        self.frames = ['\n'.join(lines) for lines in frames_lines]

def main(stdscr):
    curses.use_default_colors()

    sprites = SpriteGroup(0, stdscr)
    sprites.add(Starfield(0, stdscr))
    sprites.add(Trail(0, stdscr))
    sprites.add(Cat(0, stdscr))

    frame = 0
    while True:
        stdscr.clear()

        sprites.draw(frame, stdscr)

        stdscr.refresh()

        time.sleep(framerate)
        frame += 1

if __name__ == '__main__':
    try:
        curses.wrapper(main)
    except KeyboardInterrupt:
        pass