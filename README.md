# nyaascii
### Animated ASCII Nyan Cat

This is a quick for-fun project to recreate the [Nyan Cat](https://en.wikipedia.org/wiki/Nyan_Cat) GIF in ASCII, suitable for use on dumb terminals like the classic VT100.

Requires Python 3 and a unix-like platform. Just run the included nyaasci.py file to start.

Thanks to [Faye Archip](https://www.fayearchip.name/) for the idea!

### Known issues
* Generally crashes on terminal resize, I was too lazy to implement resize handling